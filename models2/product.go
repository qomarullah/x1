package models2

var productName = "test"

func GetProduct() string {
	return productName
}

type Product struct {
	id    int
	name  string
	price int
}

func NewProduct(id int, name string, price int) Product {
	return Product{
		id:    id,
		name:  name,
		price: price,
	}
}

func (p Product) GetName() string {
	return p.name
}
