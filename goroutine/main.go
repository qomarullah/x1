package main

import (
	"fmt"
	"strconv"
	"sync"
	"time"
)

func main() {

	var wg sync.WaitGroup
	wg.Add(2)

	//penggunaan wg
	var result = make(map[string]int)

	go insertDB(1, &wg, result)
	go putKafka(1, &wg)

	wg.Add(100)
	for i := 0; i < 100; i++ {
		go insertDB(i, &wg, result)
	}

	wg.Wait()
}

func insertDB(id int, wg *sync.WaitGroup, result map[string]int) {
	time.Sleep(1 * time.Millisecond)
	fmt.Println("insert db", id)
	var mutex sync.Mutex
	mutex.Lock()
	result["xxx"+strconv.Itoa(id)] = id
	mutex.Unlock()
	wg.Done()
}

func putKafka(id int, wg *sync.WaitGroup) {
	fmt.Println("put kafka", id)
	wg.Done()
}
