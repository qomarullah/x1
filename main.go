package main

import (
	"fmt"
	"html"
	"log"
	"net/http"
	"os"
)

func main() {

	http.HandleFunc("/status", status)
	http.HandleFunc("/", bar)

	port := ":1234"
	if os.Getenv("PORT") != "" {
		port = os.Getenv("PORT")
	}
	fmt.Println("running server..." + port)
	log.Fatal(http.ListenAndServe(port, nil))
}

func bar(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello, %q", html.EscapeString(r.URL.Path))
}

func status(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "OK")
}
