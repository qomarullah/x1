package main

import (
	"fmt"
	"reflect"
	"time"
)

func mainx() {
	fmt.Println("halo dunia")
	testVariable := 0

	fmt.Println("variable", testVariable)

	var b = "1"
	fmt.Println("variable", reflect.TypeOf(b), &b)
	c := &b
	fmt.Println("variable", reflect.TypeOf(c), *c)

	//anonymous
	func() {
		fmt.Println("anonymous func")
	}()

	//multiple return
	out1, out2 := reverse("satu", "dua")
	fmt.Println(out1, out2)

	//ignore
	out3, _ := reverse("satu", "dua")
	fmt.Println(out3)

	//resp := callAPI("http-b")

	//access public variable/func
	/*fmt.Println(models.GetProduct())

	//style 1
	p := models.Product{Id: 1, Name: "ok", Price: 10000}
	fmt.Println(p.GetName())
	p.Name = "ganti"
	fmt.Println(p.GetName())

	//style 2 hidden
	p2 := models2.NewProduct(1, "denom 1000", 2000)
	fmt.Println(p2.GetName())

	for {
	}*/

	//goroutine
	//insert DB, queue, call api
	//go callAPI("http-a")

	resp := callAPI("http-b")
	fmt.Println("response", resp)

	//channel
	respChannel := make(chan string)
	go callAPI2("http-A", respChannel)

	respChannelB := make(chan string)
	go callAPI2("http-B", respChannelB)

	resultA := <-respChannel
	fmt.Println(resultA)

	resultB := <-respChannelB
	fmt.Println(resultB)

}
func callAPI2(url string, resp chan string) {
	time.Sleep(1 * time.Second)
	fmt.Println("process call.." + url)
	resp <- "OK.." + url

}
func callAPI(url string) (resp string) {
	fmt.Println("process call..")
	resp = "OK"
	return resp
}
func reverse(a, b string) (string, string) {
	return b, a
}

func cetak(tag string, total int, sleep int) {

	for i := 0; i < total; i++ {
		time.Sleep(time.Duration(sleep) * time.Microsecond)
		fmt.Println(tag, i)
	}
}
