package main

import (
	"testing"
)

func Benchmark_reserve(b *testing.B) {
	for n := 0; n < b.N; n++ {
		reverse("1", "2")
	}
}
func Test_reverse(t *testing.T) {
	type args struct {
		a string
		b string
	}
	tests := []struct {
		name  string
		args  args
		want  string
		want1 string
	}{
		// TODO: Add test cases.
		{"positive", args{"1", "2"}, "2", "1"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1 := reverse(tt.args.a, tt.args.b)
			if got != tt.want {
				t.Errorf("reverse() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("reverse() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}
