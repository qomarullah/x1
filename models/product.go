package models

var productName = "test"

func GetProduct() string {
	return productName
}

type Product struct {
	Id    int
	Name  string
	Price int
}

func (p Product) GetName() string {
	return p.Name
}
